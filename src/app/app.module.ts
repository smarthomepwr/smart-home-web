import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { MainComponent } from './components/main/main.component';
import { RouterModule } from '@angular/router';
import { routes } from './app.routes';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './material.module';
import { ReactiveFormsModule } from '@angular/forms';
import { RoomsService } from './services/rooms.service';
import { RoomsComponent } from './components/rooms/rooms.component';
import { RoomComponent } from './components/room/room.component';
import { RoomDetailsComponent } from './components/room-details/room-details.component';
import { ChartsModule } from 'ng2-charts';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { SensorsService } from './services/sensors.service';
import { ScenesComponent } from './components/scenes/scenes.component';
import { SceneDialogComponent } from './components/scene-dialog/scene-dialog.component';
import { RoomSensorDialogComponent } from './components/room-sensor-dialog/room-sensor-dialog.component';
import { SensorDialogComponent } from './components/sensor-dialog/sensor-dialog.component';
import { RoomDialogComponent } from './components/room-dialog/room-dialog.component';
import { HttpClient, HttpHandler, HttpClientModule } from '@angular/common/http';
import { ScenesService } from './services/scenes.service';
import { SceneComponent } from './components/scene/scene.component';
import { HomeOverviewComponent } from './components/home-overview/home-overview.component';
import { WeatherService } from './services/weather.service';


@NgModule({
  declarations: [
    AppComponent,
    MainComponent,
    RoomsComponent,
    RoomComponent,
    RoomDetailsComponent,
    ScenesComponent,
    SceneDialogComponent,
    SensorDialogComponent,
    RoomSensorDialogComponent,
    RoomDialogComponent,
    ScenesComponent,
    SceneComponent,
    HomeOverviewComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(routes),
    BrowserAnimationsModule,
    MaterialModule,
    ReactiveFormsModule,
    ChartsModule,
    MatSlideToggleModule,
    HttpClientModule
  ],
  providers: [
    RoomsService,
    SensorsService,
    ScenesService,
    WeatherService
  ],
  entryComponents: [
    SceneDialogComponent,
    RoomSensorDialogComponent,
    SensorDialogComponent,
    RoomDialogComponent
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

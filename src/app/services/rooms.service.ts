import { Injectable } from '@angular/core';
import { rooms, KitchenData, roomsDetails } from '../../shared/dataMock/dataMock';
import { HttpClient } from '@angular/common/http';
import * as _ from 'lodash';


@Injectable()
export class RoomsService {

  constructor(
    private http: HttpClient
  ) { }

  saveRoom(data) {
    return this.http.post('./api/v1/rooms', data);
  }

  fetchRoomDetails(id: number) {
    return this.http.get(`./api/v1/rooms/${id}`);
  }

  fetchRoomsNames() {
    return this.http.get('./api/v1/rooms');
  }

  addSensorToRoom(sensorId: number, roomId: number) {
    return this.http.post(`./api/v1/rooms/${roomId}/addsensor/${sensorId}`, {});
  }
}

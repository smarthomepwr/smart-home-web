import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import * as _ from 'lodash';
import { Observable } from 'rxjs/Observable';


@Injectable()
export class ScenesService {

  constructor(
    private http: HttpClient
  ) { }

  fetchScenes(): Observable<Scene[]> {
    return this.http.get('./api/v1/events')
      .map((scenes: any[]) => scenes.map((scene) => this.parseScene(scene))) as Observable<Scene[]>;
  }

  createScene(data: Scene) {
    const body = {
      trigger: data.triggerId,
      trigger_value: data.triggerValue,
      trigger_type: data.triggerType,
      resolver: data.resolverId,
      trigger_action: data.triggerAction,
      resolver_value: data.resolverValue
    };

    return this.http.post('./api/v1/events', body);
  }

  removeScene(sceneId): Observable<any> {
    return this.http.delete(`./api/v1/events/${sceneId}`);
  }

  parseScene(scene) {
    return {
      id: scene.id,
      triggerId: scene.trigger,
      triggerValue: scene.trigger_value,
      triggerType: scene.trigger_type,
      triggerAction: scene.trigger_action,
      resolverId: scene.resolver,
      resolverValue: scene.resolver_value
    };
  }

}

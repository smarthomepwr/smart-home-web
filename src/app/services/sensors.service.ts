import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import { HttpClient } from '@angular/common/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';

@Injectable()
export class SensorsService {

  constructor(
    private http: HttpClient
  ) { }

  fetchAllSensors(): Observable<any[]> {
    return this.http.get('./api/v1/sensors').do(console.log) as Observable<any[]>;
  }

  changeState(sensorId, state) {
    if (state === 'ON') {
      return this.http.put(`./api/v1/sensors/${sensorId}/on`, {});
    } else if (state === 'OFF') {
      return this.http.put(`./api/v1/sensors/${sensorId}/off`, {});
    }
  }

  fetchSensorData(sensorId) {
    return this.http.get(`./api/v1/sensors/${sensorId}/data`);
  }

}

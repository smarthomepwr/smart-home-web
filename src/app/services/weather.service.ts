import { Injectable } from '@angular/core';
import { rooms, KitchenData, roomsDetails } from '../../shared/dataMock/dataMock';
import { HttpClient } from '@angular/common/http';
import * as _ from 'lodash';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';


@Injectable()
export class WeatherService {

  API_KEY = '814f5a293658154341ef327e16dc87cf';
  position = new BehaviorSubject(null);

  constructor(
    private http: HttpClient
  ) { }

  getPosition() {
    console.log('getPos')
    navigator.geolocation.getCurrentPosition((pos) => {
      const lat = pos.coords.latitude;
      const lng = pos.coords.longitude;
      this.position.next({ lat, lng });
    });
  }

  getWeather() {
    return this.http.get(`https://samples.openweathermap.org/data/2.5/weather?q=Wroclaw,pl&appid=${this.API_KEY}`);
  }
}

import { Routes } from '@angular/router';
import { MainComponent } from './components/main/main.component';
import { RoomsComponent } from './components/rooms/rooms.component';
import { RoomDetailsComponent } from './components/room-details/room-details.component';
import { ScenesComponent } from './components/scenes/scenes.component';
import { HomeOverviewComponent } from './components/home-overview/home-overview.component';

export const routes: Routes = [
  {
    path: 'home', component: MainComponent, children: [
      { path: 'room/:roomId', component: RoomDetailsComponent },
      { path: 'overview', component: HomeOverviewComponent },
      { path: 'scenes', component: ScenesComponent },
      { path: '', component: RoomsComponent },
    ]
  },
  { path: '**', redirectTo: '/home' },
];

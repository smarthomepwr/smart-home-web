import { TestBed, async } from '@angular/core/testing';
import { AppComponent } from './app.component';
import { RouterTestingModule } from '@angular/router/testing';
import { routes } from './app.routes';
import { MainComponent } from './components/main/main.component';
import { fakeAsync } from '@angular/core/testing';
import { Router } from '@angular/router';
import { tick } from '@angular/core/testing';
import { Location } from '@angular/common';
import { RoomsComponent } from './components/rooms/rooms.component';
import { MaterialModule } from './material.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RoomsService } from './services/rooms.service';
import { RoomDetailsComponent } from './components/room-details/room-details.component';
import { RoomComponent } from './components/room/room.component';
describe('AppComponent', () => {
  let location: Location;
  let router: Router;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent,
        MainComponent,
        RoomsComponent,
        RoomDetailsComponent,
        RoomComponent
      ],
      imports: [
        RouterTestingModule.withRoutes(routes),
        MaterialModule,
        BrowserAnimationsModule
      ],
      providers: [
        RoomsService
      ]
    }).compileComponents();
    router = TestBed.get(Router);
    location = TestBed.get(Location);
  }));
  it('should create the app', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));
  it(`should redirect to 'home'`, fakeAsync(() => {
    const fixture = TestBed.createComponent(AppComponent);
    router.initialNavigation();
    router.navigate(['']);
    tick();
    expect(location.path()).toBe('/home');
  }));
});


import { NgModule } from '@angular/core';
// tslint:disable-next-line:max-line-length
import { MatButtonModule, MatIconModule, MatSelectModule, MatOptionModule, MatDialogModule, MatInputModule, MatDatepickerModule, MatNativeDateModule, MatListModule, MatMenuModule, MatAutocompleteModule, MatStepperModule, MatTableModule, MatCheckboxModule, MatProgressSpinnerModule, MatTooltipModule, MatRadioModule, MatSidenavModule, MatToolbarModule, MatCardModule } from '@angular/material';
import { CdkTableModule } from '@angular/cdk/table';

@NgModule({
  imports: [
    MatButtonModule,
    MatIconModule,
    MatSelectModule,
    MatOptionModule,
    MatDialogModule,
    MatInputModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatListModule,
    MatMenuModule,
    MatAutocompleteModule,
    MatStepperModule,
    MatTableModule,
    MatCheckboxModule,
    MatProgressSpinnerModule,
    MatTooltipModule,
    MatRadioModule,
    MatSidenavModule,
    MatToolbarModule,
    MatCardModule,
    CdkTableModule
  ],
  exports: [
    MatButtonModule,
    MatIconModule,
    MatSelectModule,
    MatOptionModule,
    MatDialogModule,
    MatInputModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatListModule,
    MatMenuModule,
    MatAutocompleteModule,
    MatStepperModule,
    MatTableModule,
    MatCheckboxModule,
    MatProgressSpinnerModule,
    MatTooltipModule,
    MatRadioModule,
    MatSidenavModule,
    MatToolbarModule,
    MatCardModule,
    CdkTableModule
  ]
})
export class MaterialModule { }

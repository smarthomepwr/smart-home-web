interface Device {
  id: string;
  prodName: string;
  name: string;
  ipAddress: string;
}

interface Sensor extends Device {
  type: 'sensor';
  dataType: TempSensor | MotionSensor;
}

interface TempSensor {
  data: {
    temperature: number;
    humidity: number;
  };
}

interface MotionSensor {
  data: {
    motion: boolean;
  };
}

interface Switch extends Device {
  type: 'switch';
  state: boolean;
}

interface BackendDevice {
  id: string;
  prodName: string;
  name: string;
  type: 'sensor' | 'switch';
  ipAddress: string;
  data: {
    motion?: string;
    temperature?: string;
    humidity?: string;
  };
  state?: boolean;
}

interface Scene {
  id?: string;
  triggerId: string;
  triggerValue: number | boolean;
  triggerType: string;
  resolverId: string;
  resolverValue: number | boolean;
  triggerAction: string;
}

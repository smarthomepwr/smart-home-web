import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RoomsComponent } from './rooms.component';
import { MaterialModule } from '../../material.module';
import { RouterTestingModule } from '@angular/router/testing';
import { routes } from '../../app.routes';
import { Router } from '@angular/router';
import { RoomsService } from '../../services/rooms.service';
import { RoomComponent } from '../room/room.component';
import { MainComponent } from '../main/main.component';
import { RoomDetailsComponent } from '../room-details/room-details.component';

describe('RoomsComponent', () => {
  let component: RoomsComponent;
  let fixture: ComponentFixture<RoomsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [RoomsComponent, RoomComponent, MainComponent, RoomDetailsComponent],
      imports: [
        MaterialModule,
        RouterTestingModule.withRoutes(routes)
      ],
      providers: [
        RoomsService
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RoomsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should fetch rooms', () => {
    expect(fixture.componentInstance.rooms.length).toBeGreaterThan(0);
  });
});

import { Component, OnInit } from '@angular/core';
import { RoomsService } from '../../services/rooms.service';
import { DataSource } from '@angular/cdk/table';
import { Observable } from 'rxjs/Observable';
import { SensorsService } from '../../services/sensors.service';
import { MatDialog, MAT_DIALOG_SCROLL_STRATEGY_PROVIDER_FACTORY } from '@angular/material';
import { SensorDialogComponent } from '../sensor-dialog/sensor-dialog.component';
import { RoomDialogComponent } from '../room-dialog/room-dialog.component';
import * as _ from 'lodash';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/take';

@Component({
  selector: 'app-rooms',
  templateUrl: './rooms.component.html',
  styleUrls: ['./rooms.component.scss']
})
export class RoomsComponent implements OnInit {

  public rooms: Array<any>;
  public sensorsDataSource: DataSource<any>;
  public columns = ['type', 'name', 'ip'];
  private roomFetchingBehavior = new BehaviorSubject<number>(0);
  constructor(
    private rs: RoomsService,
    private ss: SensorsService,
    private matDialog: MatDialog
  ) {
  }

  ngOnInit() {
    this.roomFetchingBehavior.do(console.log).switchMap((t) => this.rs.fetchRoomsNames()).subscribe(res => {
      console.log(res);
      const rooms = [];
      _.forEach(res, d => {
        rooms.push({
          'id': d.id,
          'name': d.name,
        });
      });
      this.rooms = rooms;
    });

    this.sensorsDataSource = new SensorsDataSource(this.ss);
  }

  openSensorDialog() {
    const dialogRef = this.matDialog.open(SensorDialogComponent);
  }

  openRoomDialog() {
    const roomRef = this.matDialog.open(RoomDialogComponent);
    roomRef.afterClosed().subscribe(res => {
      this.rs.saveRoom(res).take(1).subscribe((r) => this.roomFetchingBehavior.next(Math.random()));
    });
  }

}


export class SensorsDataSource extends DataSource<any> {

  constructor(
    private ss: SensorsService
  ) {
    super();
  }

  connect(): Observable<any[]> {
    return this.ss.fetchAllSensors();
  }

  disconnect() { }
}



import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { SensorsService } from '../../services/sensors.service';

@Component({
  selector: 'app-sensor-dialog',
  templateUrl: './sensor-dialog.component.html',
  styleUrls: ['./sensor-dialog.component.scss']
})
export class SensorDialogComponent implements OnInit {

  sensors: Object;
  sensorGroup: FormGroup;
  loading = false;

  constructor(
    public dialogRef: MatDialogRef<SensorDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private fb: FormBuilder,
    private sensorsService: SensorsService
  ) { }

  ngOnInit() {
    this.sensorGroup = this.fb.group({
      name: ['', Validators.required],
      ipAddress: ['', Validators.required]
    });
    this.sensorsService.fetchAllSensors().subscribe((data) => {
      this.sensors = data;
    });
  }

  autoSearch() {
    this.sensorGroup.setValue({
      name: this.sensors[0].type,
      ipAddress: this.sensors[0].address
    })
    console.log(this.sensorGroup)
  }

  add() {
    this.loading = true;
  }

}

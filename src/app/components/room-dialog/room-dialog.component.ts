import { Component, OnInit, Inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-room-dialog',
  templateUrl: './room-dialog.component.html',
  styleUrls: ['./room-dialog.component.scss']
})
export class RoomDialogComponent implements OnInit {

  RoomForm: FormGroup;


  constructor(
    public dialogRef: MatDialogRef<RoomDialogComponent>,
    @Inject(FormBuilder) fb: FormBuilder) {
    this.RoomForm = fb.group({
      roomName: ['', Validators.required],
      roomDesc: ['', Validators.required],
      roomFloorNumber: ['', Validators.required],
    });
  }

  ngOnInit() {
  }

  save() {
    const result = {
      name: this.RoomForm.controls.roomName.value,
      description: this.RoomForm.controls.roomDesc.value,
    };

    this.dialogRef.close(result);

  }

}

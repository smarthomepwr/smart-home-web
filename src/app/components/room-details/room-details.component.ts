import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { RoomsService } from '../../services/rooms.service';
import { lineChartColors, lineChartLegend, lineChartType, lineChartOptions } from './charts-config';
import * as _ from 'lodash';
import { MatDialog } from '@angular/material';
import { RoomSensorDialogComponent } from '../room-sensor-dialog/room-sensor-dialog.component';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { SensorsService } from '../../services/sensors.service';
import { IfObservable } from 'rxjs/observable/IfObservable';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/combineLatest';

@Component({
  selector: 'app-room-details',
  templateUrl: './room-details.component.html',
  styleUrls: ['./room-details.component.scss']
})
export class RoomDetailsComponent implements OnInit {

  roomData: any;
  temperatureData: Array<any>;
  timestampLabels: Array<any>;
  lineChartOptions = lineChartOptions;
  lineChartColors = lineChartColors;
  lineChartLegend = lineChartLegend;
  lineChartType = lineChartType;
  humidityData: any;
  devices = [
  ]
  fetchBehavior = new BehaviorSubject(0);
  roomId: number;
  tempSensor;

  constructor(
    private route: ActivatedRoute,
    private roomsService: RoomsService,
    private sensorsService: SensorsService,
    private dialog: MatDialog,

  ) { }

  ngOnInit() {
    this.fetchBehavior.switchMap(() => this.route.params).switchMap(param => {
      return this.roomsService.fetchRoomDetails(param['roomId']);
    }).do(console.log)
      .switchMap((rd) => {
        let temperatureSensor;
        this.roomData = rd;
        this.roomData.sensors.forEach(sensor => {
          if (sensor.type === 'relay') {
            this.devices.push(sensor);
          }
          if (sensor.type === 'temperature') {
            temperatureSensor = sensor;
          }
        });
        return temperatureSensor ? this.sensorsService.fetchSensorData(temperatureSensor.id) : Observable.of(null);
      }).subscribe((tempSensor) => {
        this.generateChartData(tempSensor);
        this.tempSensor = tempSensor;
      });
  }

  changeState(deviceId, state) {
    this.sensorsService.changeState(deviceId, state).subscribe();
  }

  generateChartData(data) {
    const temperature = [];
    const timestampLabels = [];
    const humidity = [];
    _.forEach(data.slice(Math.max(data.length - 10, 1)), (d) => {
      temperature.push(d.temperature);
      humidity.push(d.humidity);
      timestampLabels.push(
        new Date(d.time).getDate() + '/' + new Date(d.time).getMonth() + ' ' + new Date(d.time).getHours().toLocaleString('pl') + ':' + (new Date(d.time).getMinutes() < 10 ? '0' + new Date(d.time).getMinutes().toLocaleString('pl') : new Date(d.time).getMinutes().toLocaleString('pl')));
    });
    this.temperatureData = [{ data: temperature, label: 'temperature' }];
    this.humidityData = [{ data: humidity, label: 'humidity' }];
    this.timestampLabels = timestampLabels;
  }

  openRoomSensorsDialog() {
    const dialogRef = this.dialog.open(RoomSensorDialogComponent);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        console.log(result);
        this.route.params.switchMap(
          (params) => this.roomsService.addSensorToRoom(result, params['roomId'])).subscribe((c) => this.fetchBehavior.next(1));
      }
    });
  }


}

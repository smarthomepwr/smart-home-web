import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormBuilder, FormGroup } from '@angular/forms';
import * as _ from 'lodash';
@Component({
  selector: 'app-scene-dialog',
  templateUrl: './scene-dialog.component.html',
  styleUrls: ['./scene-dialog.component.scss']
})
export class SceneDialogComponent implements OnInit {

  public sceneGroup: FormGroup;

  constructor(
    public dialogRef: MatDialogRef<SceneDialogComponent>,
    private formBuilder: FormBuilder,
    @Inject(MAT_DIALOG_DATA) public data
  ) { }

  ngOnInit() {
    this.sceneGroup = this.formBuilder.group({
      triggerId: '',
      triggerValue: 0,
      triggerType: '',
      triggerAction: '',
      resolverId: '',
      resolverValue: 0,
    });

    this.sceneGroup.controls.triggerId.valueChanges.subscribe((value) => {
      const sensor = _.find(this.data.sensors, ref => ref.id === value);
      if (sensor.type === 'motion') {
        this.sceneGroup.controls.triggerValue.disable();
        this.sceneGroup.controls.triggerAction.disable();
        this.sceneGroup.controls.triggerType.disable();
      }
      if (sensor.type === 'temperature') {
        this.sceneGroup.controls.triggerValue.enable();
        this.sceneGroup.controls.triggerType.enable();
        this.sceneGroup.controls.triggerAction.disable();

      }
    });
  }


  save() {
    const motion = _.find(this.data.sensors, ref => ref.id === this.sceneGroup.controls.triggerId.value).type === 'motion';
    const scene: Scene = {
      triggerId: this.sceneGroup.controls.triggerId.value,
      triggerType: motion ? 'motion' : this.sceneGroup.controls.triggerType.value,
      triggerValue: motion ? 1 : Number(this.sceneGroup.controls.triggerValue.value),
      resolverId: this.sceneGroup.controls.resolverId.value,
      resolverValue: Number(this.sceneGroup.controls.resolverValue.value),
      triggerAction: motion ? '==' : this.sceneGroup.controls.triggerAction.value
    };
    console.log(scene);
    this.dialogRef.close({
      scene
    });
  }

}

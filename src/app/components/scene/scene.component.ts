import { Component, OnInit, Input, OnChanges } from '@angular/core';
import * as _ from 'lodash';
import { ScenesService } from '../../services/scenes.service';
@Component({
  selector: 'app-scene',
  templateUrl: './scene.component.html',
  styleUrls: ['./scene.component.scss']
})
export class SceneComponent implements OnChanges {

  @Input() scene: Scene;
  @Input() sensors: Array<any>;
  @Input() switches: Array<any>;

  get triggerType() {
    switch (this.scene.triggerType) {
      case 'temperature':
        return 'Temperatura';
      case 'humidity':
        return 'Wilgotność';
      case 'motion':
        return 'Ruch';
    }
  }

  get resolverValue() {
    switch (this.scene.resolverValue) {
      case 1:
        return 'Włącz';
      case 0:
        return 'Wyłącz';
    }
  }

  resolverName: string;
  triggerName: string;

  constructor(
    private ss: ScenesService
  ) { }

  ngOnChanges() {
    if (this.sensors && this.switches) {
      this.resolverName = _.find(this.switches, ref => ref.id === this.scene.resolverId).name;
      this.triggerName = _.find(this.sensors, ref => ref.id === this.scene.resolverId).name;
    }
  }

  removeScene() {
    this.ss.removeScene(this.scene.id).take(1).subscribe();
  }

}

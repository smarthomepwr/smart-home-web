import { Component, OnInit } from '@angular/core';
import { RoomsService } from '../../services/rooms.service';
import { Location } from '@angular/common';
import { Router } from '@angular/router';
import * as _ from 'lodash';
@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {
  overview = false;
  constructor(
    private router: Router
  ) { }

  ngOnInit() {
    this.overview = _.findIndex(window.location.href.split('/'), ref => ref === 'overview') !== -1;
    console.log(this.overview);
    this.router.events.subscribe((event) => {
      this.overview = _.findIndex(window.location.href.split('/'), ref => ref === 'overview') !== -1;
      console.log(this.overview)
    });
  }

  goBack() {
    this.router.navigate(['']);
  }

}

import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { SceneDialogComponent } from '../scene-dialog/scene-dialog.component';
import { ScenesService } from '../../services/scenes.service';
import { SensorsService } from '../../services/sensors.service';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Component({
  selector: 'app-scenes',
  templateUrl: './scenes.component.html',
  styleUrls: ['./scenes.component.scss']
})
export class ScenesComponent implements OnInit {

  scenes: Scene[];
  sensors: any[];
  switches: any[];
  fetchBehavior: BehaviorSubject<boolean> = new BehaviorSubject(true);
  constructor(
    private dialog: MatDialog,
    private scenesService: ScenesService,
    private sensorsService: SensorsService
  ) { }

  ngOnInit() {
    this.fetchBehavior.switchMap(() => this.sensorsService.fetchAllSensors()).take(1).subscribe((sensors) => {
      console.log(sensors);
      this.sensors = sensors.filter(sen => sen.type === 'motion' || sen.type === 'temperature');
      this.switches = sensors.filter(sen => sen.type === 'relay');
    });
    this.scenesService.fetchScenes().subscribe((scenes) => {
      this.scenes = scenes;
    });
  }

  openSceneCreator() {
    const dialogRef = this.dialog.open(SceneDialogComponent, {
      data: {
        sensors: this.sensors,
        switches: this.switches
      }
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        const scene: Scene = result.scene;
        this.scenesService.createScene(scene).subscribe((res) => this.fetchBehavior.next(true));
      }
    });
  }

}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RoomComponent } from './room.component';
import { MaterialModule } from '../../material.module';
import { RouterTestingModule } from '@angular/router/testing';
import { routes } from '../../app.routes';
import { MainComponent } from '../main/main.component';
import { RoomDetailsComponent } from '../room-details/room-details.component';
import { RoomsComponent } from '../rooms/rooms.component';

describe('RoomComponent', () => {
  let component: RoomComponent;
  let fixture: ComponentFixture<RoomComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [RoomComponent, MainComponent, RoomDetailsComponent, RoomsComponent],
      imports: [MaterialModule, RouterTestingModule.withRoutes(routes)]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RoomComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

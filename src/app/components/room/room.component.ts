import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { lineChartColors, lineChartLegend, lineChartType, lineChartOptions } from '../room-details/charts-config';
import { RoomsService } from '../../services/rooms.service';
import * as _ from 'lodash';
@Component({
  selector: 'app-room',
  templateUrl: './room.component.html',
  styleUrls: ['./room.component.scss']
})
export class RoomComponent implements OnInit {

  @Input('room') room;

  roomData: any;
  temperatureData: Array<any>;
  timestampLabels: Array<any>;
  lineChartOptions = lineChartOptions;
  lineChartColors = lineChartColors;
  lineChartLegend = lineChartLegend;
  lineChartType = lineChartType;
  humidityData: any;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private roomsService: RoomsService
  ) { }

  ngOnInit() {
    this.roomsService.fetchRoomDetails(this.room.id).subscribe((rd) => this.roomData = rd);
  }

  navigateToRoom() {
    this.router.navigate(['/home', 'room', this.room.id]);
  }

  //generateChartData() {
  //  let temperature = [];
  //  let timestampLabels = [];
  //  let humidity = [];
  //  _.forEach(this.roomData.devices[0].data, (d) => {
  //    temperature.push(d.temperature);
  //    humidity.push(d.humidity);
  //    timestampLabels.push(d.timestamp.getHours().toLocaleString('pl') + ':' + (d.timestamp.getMinutes() < 10 ? '0' + d.timestamp.getMinutes().toLocaleString('pl') : d.timestamp.getMinutes().toLocaleString('pl')));
  //  })
  //  this.temperatureData = [{ data: temperature, label: 'temperature' }]
  //  this.humidityData = [{ data: humidity, label: 'humidity' }];
  //  this.timestampLabels = timestampLabels;
  //}

}

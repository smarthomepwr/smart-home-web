import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RoomSensorDialogComponent } from './room-sensor-dialog.component';

describe('RoomSensorDialogComponent', () => {
  let component: RoomSensorDialogComponent;
  let fixture: ComponentFixture<RoomSensorDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RoomSensorDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RoomSensorDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

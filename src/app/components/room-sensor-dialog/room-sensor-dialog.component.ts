import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import { FormControl, Validators } from '@angular/forms';
import { SensorsService } from '../../services/sensors.service';

@Component({
  selector: 'app-room-sensor-dialog',
  templateUrl: './room-sensor-dialog.component.html',
  styleUrls: ['./room-sensor-dialog.component.scss']
})
export class RoomSensorDialogComponent implements OnInit {

  sensorId: FormControl = new FormControl('', Validators.required);
  sensors: Array<any>;
  constructor(
    public dialogRef: MatDialogRef<RoomSensorDialogComponent>,
    private ss: SensorsService
  ) { }

  ngOnInit() {
    this.ss.fetchAllSensors().subscribe(sensors => this.sensors = sensors);
  }

  save() {
    this.dialogRef.close(this.sensorId.value);
  }



}

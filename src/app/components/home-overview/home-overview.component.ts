import { Component, OnInit } from '@angular/core';
import { SensorsService } from '../../services/sensors.service';
import { combineLatest } from 'rxjs/observable/combineLatest';
import { Observable } from 'rxjs/Observable';
import * as _ from 'lodash';
import { WeatherService } from '../../services/weather.service';

@Component({
  selector: 'app-home-overview',
  templateUrl: './home-overview.component.html',
  styleUrls: ['./home-overview.component.scss']
})
export class HomeOverviewComponent implements OnInit {

  public sensors;

  constructor(
    private sensorService: SensorsService,
    private weatherService: WeatherService
  ) { }

  ngOnInit() {
    // this.weatherService.getPosition();
    // this.weatherService.position.switchMap((position) => {
    //   if (position) {
    //     return this.weatherService.getWeather(position.lat, position.lng);
    //   } else {
    //     return Observable.of(null)
    //   }
    // })
    //   .subscribe((weather) => {
    //     console.log(weather)
    //   })

    // this.weatherService.getWeather().subscribe((weather) => console.log(weather))
    this.sensorService
      .fetchAllSensors()
      .switchMap((sensors) => {
        return combineLatest(
          sensors.map((sensor) =>
            sensor.type === 'temperature' ? this.sensorService.fetchSensorData(sensor.id) : Observable.of(null)
          )
        );
      })
      .subscribe(s => {
        this.sensors = s.filter((d) => !!d).map((sensorData) => {
          return sensorData[sensorData.length - 1];
        });
      });
  }

}

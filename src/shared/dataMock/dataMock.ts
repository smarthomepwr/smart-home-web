export const rooms = [{
    id: 0,
    name: 'Kuchnia',
    sensors: 3,
    currentData: {
        temperature: 22.3,
        humidity: 45,
        timestamp: new Date()
    }
}, {
    id: 1,
    name: 'Łazienka',
    sensors: 3,
    currentData: {
        temperature: 25.3,
        humidity: 70,
        timestamp: new Date()
    }
}, {
    id: 2,
    name: 'Sypialnia',
    sensors: 3,
    currentData: {
        temperature: 21.3,
        humidity: 30,
        timestamp: new Date()
    }
}, {
    id: 3,
    name: 'Salon',
    sensors: 3,
    currentData: {
        temperature: 22.3,
        humidity: 55,
        timestamp: new Date()
    }
},];

interface DeviceData {
    temperature: number;
    timestamp: Date;
    humidity: number;
}

interface roomData {
    id: number;
    roomName: string;
    devices: Device[];
}

interface Device {
    id: number;
    name: string;
    state: boolean;
    data: DeviceData[]
}



export const BathroomnData: roomData = {
    id: 1,
    roomName: 'Łazienka',
    devices: [{
        id: 1,
        name: 'Czujnik 1',
        state: true,
        data: [
            {
                temperature: 24.3,
                timestamp: new Date(new Date().getTime() - 1000 * 60 * 60),
                humidity: 56,
            },
            {
                temperature: 23.9,
                timestamp: new Date(new Date().getTime() - 1000 * 60 * 50),
                humidity: 56,
            },
            {
                temperature: 25.5,
                timestamp: new Date(new Date().getTime() - 1000 * 60 * 40),
                humidity: 76,
            },
            {
                temperature: 22.8,
                timestamp: new Date(new Date().getTime() - 1000 * 60 * 30),
                humidity: 95,
            },
            {
                temperature: 23.8,
                timestamp: new Date(new Date().getTime() - 1000 * 60 * 20),
                humidity: 100,
            }]
    }]
}

export const BedroomData: roomData = {
    id: 2,
    roomName: 'Sypialnia',
    devices: [{
        id: 1,
        name: 'Czujnik 1',
        state: true,
        data: [
            {
                temperature: 22.3,
                timestamp: new Date(new Date().getTime() - 1000 * 60 * 60),
                humidity: 36,
            },
            {
                temperature: 23.9,
                timestamp: new Date(new Date().getTime() - 1000 * 60 * 50),
                humidity: 56,
            },
            {
                temperature: 21.5,
                timestamp: new Date(new Date().getTime() - 1000 * 60 * 40),
                humidity: 46,
            },
            {
                temperature: 26.8,
                timestamp: new Date(new Date().getTime() - 1000 * 60 * 30),
                humidity: 35,
            },
            {
                temperature: 21.8,
                timestamp: new Date(new Date().getTime() - 1000 * 60 * 20),
                humidity: 59,
            }]
    }]
}

export const LivingroomData: roomData = {
    id: 3,
    roomName: 'Salon',
    devices: [{
        id: 1,
        name: 'Czujnik 1',
        state: true,
        data: [
            {
                temperature: 21.3,
                timestamp: new Date(new Date().getTime() - 1000 * 60 * 60),
                humidity: 21,
            },
            {
                temperature: 26.9,
                timestamp: new Date(new Date().getTime() - 1000 * 60 * 50),
                humidity: 56,
            },
            {
                temperature: 23.5,
                timestamp: new Date(new Date().getTime() - 1000 * 60 * 40),
                humidity: 47,
            },
            {
                temperature: 24.8,
                timestamp: new Date(new Date().getTime() - 1000 * 60 * 30),
                humidity: 45,
            },
            {
                temperature: 27.8,
                timestamp: new Date(new Date().getTime() - 1000 * 60 * 20),
                humidity: 29,
            }]
    }]
}

export const KitchenData: roomData = {
    id: 0,
    roomName: 'Kuchnia',
    devices: [{
        id: 1,
        name: 'Czujnik 1',
        state: true,
        data: [
            {
                temperature: 22.3,
                timestamp: new Date(new Date().getTime() - 1000 * 60 * 60),
                humidity: 36,
            },
            {
                temperature: 21.9,
                timestamp: new Date(new Date().getTime() - 1000 * 60 * 50),
                humidity: 56,
            },
            {
                temperature: 25.5,
                timestamp: new Date(new Date().getTime() - 1000 * 60 * 40),
                humidity: 46,
            },
            {
                temperature: 21.8,
                timestamp: new Date(new Date().getTime() - 1000 * 60 * 30),
                humidity: 35,
            },
            {
                temperature: 22.8,
                timestamp: new Date(new Date().getTime() - 1000 * 60 * 20),
                humidity: 69,
            }]
    }]
}

export const roomsDetails = [KitchenData, BathroomnData, BedroomData, LivingroomData]
